<?php

namespace Modules\ActiveCampaign\Listeners;

use Modules\ActiveCampaign\Entities\ActiveCampaign;
use Modules\Cart\Entities\CartStorage;
use Modules\Order\Entities\Order;

class CreateOrUpdateOrder
{
    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle($event)
    {
        if(setting('activeCampaign_enabled')){
            $order = $event instanceof Order ? $event : $event->order;
            $ac = new ActiveCampaign();
            $products = [];
            $customerId = 0;
            $connectionId = $ac->connectionId();

            foreach ($order->products as $key => $value) {
                $products[$key]['externalid'] = $value->product->id;
                $products[$key]['name'] = $value->product->name;
                $products[$key]['price'] = bcmul($value->unit_price->amount(), 100);
                $products[$key]['quantity'] = $value->qty;
            };

            $customer = $ac->findCustomerByEmail($order->customer_email);
            if (isset($customer)) {
                $customerId = $customer->id;
            }
            elseif (isset($order->customer_id)) {
                $acCustomer = $ac->createCustomer([
                    'id' => $order->customer_id,
                    'email' => $order->customer_email,
                    'firstName' => $order->customer_first_name,
                    'lastName' => $order->customer_last_name,
                    'phone' => $order->customer_email,
                ]);

                if (isset($acCustomer->ecomCustomer)) {
                    $customerId = $acCustomer->ecomCustomer->id;
                }
            }

            $data['ecomOrder'] = [
                'externalid' =>  $order->id,
                'state' => ActiveCampaign::ORDER_STATE_WAITING,
                'email' => $order->customer_email,
                'orderProducts' => $products,
                'totalPrice' => bcmul($order->total->amount(), 100),
                'currency' => $order->currency,
                'connectionid' => $connectionId,
                'customerid' => $customerId,
                'externalCreatedDate' => $order->created_at->toDateTimeString()
            ];

            switch ($order->status) {
                case Order::PENDING_PAYMENT:
                    $data['ecomOrder']['state'] = ActiveCampaign::ORDER_STATE_WAITING;
                    break;
                case Order::COMPLETED:
                case Order::CANCELED:
                case Order::ON_HOLD:
                case Order::REFUNDED:
                    $data['ecomOrder']['state'] = ActiveCampaign::ORDER_STATE_COMPLETED;
                    break;
                case Order::PENDING:
                default:
                    $data['ecomOrder']['state'] = ActiveCampaign::ORDER_STATE_PENDING;
                    break;
            }

            $acOrders = $ac->listOrders([
                'connectionid' => $connectionId,
                'externalid' =>  $order->id,
            ]);

            if (empty($acOrders) && isset($order->customer_id)) {
                $cartSession = CartStorage::find("{$order->customer_id}_cart_items");
                if($cartSession) {
                    $acOrders = $ac->listOrders([
                        'connectionid' => $connectionId,
                        'externalcheckoutid' =>  "{$order->customer_id}_{$cartSession->updated_at->toDateString()}",
                    ]);
                    /*if($acOrders) {
                        $data['ecomOrder']['state'] = ActiveCampaign::ORDER_STATE_RECOVERED;
                    }*/
                }
            }

            if(empty($acOrders)) {
                $ac->createOrder($data);
            }else{
                $ac->updateOrder($acOrders[0]->id, $data);
            }
        }
    }
}
