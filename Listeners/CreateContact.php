<?php

namespace Modules\ActiveCampaign\Listeners;

use Modules\ActiveCampaign\Entities\ActiveCampaign;
use Modules\User\Entities\User;

class CreateContact
{
    /**
     * Handle the event.
     * @param $event
     *
     * @return void
     */
    public function handle($event)
    {
        if(setting('activeCampaign_enabled')){
            $user = $event instanceof User ? $event : $event->user;

            $ac = new ActiveCampaign();

            $data['id'] = $user->id;
            $data['email'] = $user->email;
            $data['firstName'] = $user->first_name;
            $data['lastName'] = $user->last_name;
            $data['phone'] = $user->phone;

            $ac->createCustomer($data);
        }
    }
}
