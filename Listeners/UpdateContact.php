<?php

namespace Modules\ActiveCampaign\Listeners;

use Modules\ActiveCampaign\Entities\ActiveCampaign;
use Modules\User\Entities\User;

class UpdateContact
{
    /**
     * Handle the event.
     * @param $event
     *
     * @return void
     */
    public function handle($event)
    {
        if(setting('activeCampaign_enabled')){
            $user = $event instanceof User ? $event : $event->user;

            $ac = new ActiveCampaign();

            $ac->createContact($user->email, $user->first_name, $user->last_name, $user->phone, true);
        }
    }
}
