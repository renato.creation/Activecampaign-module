<?php

namespace Modules\ActiveCampaign\Entities;

use Ixudra\Curl\Facades\Curl;

class Connection
{
    public $baseUrl;
    public $key;

    public function __construct()
    {
        $this->baseUrl = setting('activeCampaign_url');
        $this->key = setting('activeCampaign_key');
    }

    public function curl($action, $method = 'POST', $values = false)
    {
        $url = $this->baseUrl . '/api/3/' . $action;

        $response = Curl::to($url)
            ->withHeader('accept:application/json')
            ->withHeader('Api-Token:' . $this->key)
            ->withData($values)
            ->asJson();

        switch ($method) {
            case 'POST':
                $response = $response->post();
                break;
            case 'DELETE':
                $response = $response->delete();
                break;
            case 'PUT':
                $response = $response->put();
                break;
            default:
                $response = $response->get();
                break;
        }

        if(is_null($response)){
            return 'false';
        }

        return $response;
    }


}