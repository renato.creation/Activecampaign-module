<?php

namespace Modules\ActiveCampaign\Entities;

use Modules\ActiveCampaign\Entities\Connection;
use Modules\Media\Entities\File;
use Modules\Setting\Entities\Setting;

class ActiveCampaign extends Connection
{
    const ORDER_STATE_PENDING = 0;
    const ORDER_STATE_COMPLETED = 1;
    const ORDER_STATE_ABANDONED = 2;
    const ORDER_STATE_RECOVERED = 3;
    const ORDER_STATE_WAITING = 4;

    public function connectionId()
    {
        if (setting('activeCampaign_connectionId')) {
            return setting('activeCampaign_connectionId');
        }

        //CREATE INTEGRATION CONNECTION
        $data['connection'] = [
            'service' => 'Ecommerce',
            'externalid' => setting('store_name'),
            'name' => setting('store_name'),
            'logoUrl' => File::findOrNew(setting('activeCampaign_header_logo'))->path,
            'linkUrl' => route('home')
        ];

        $result = $this->curl('connections', 'POST', $data);

        Setting::set('activeCampaign_connectionId', $result->connection->id);

        return $result->connection->id;
    }

    public function createCustomer($customer, $acceptsMarketing = 0)
    {
        $this->createContact(
            $customer['email'],
            $customer['firstName'] ?? null,
            $customer['lastName'] ?? null,
            $customer['phone'] ?? null,
            true
        );

        if (!isset($customer['id']) || empty($customer['id'])) {
            return null;
        }

        $connectionId = $this->connectionId();

        //CREATE E-COMMERCE CUSTOMER
        $data_customer['ecomCustomer'] = [
            'connectionid' => $connectionId,
            'externalid' => $customer['id'],
            'email' => $customer['email'],
            'acceptsMarketing' => $acceptsMarketing
        ];

        return $this->curl('ecomCustomers', 'POST', $data_customer);
    }

    public function listCustomer($filters = [])
    {
        $data['filters'] = array_merge([
            'connectionid' => $this->connectionId()
        ], $filters);

        $result =  $this->curl('ecomCustomers', 'GET', $data);

        if (empty($result)) return [];

        return $result->ecomCustomers;
    }

    public function findCustomerByEmail($email)
    {
        $customers = $this->listCustomer(['email' => $email]);

        if (empty($customers)) return null;

        return $customers[0];
    }

    public function createContact($email, $firstName = null, $lastName = null, $phone = null, $updateIfExists = true)
    {
        if ($acContact = $this->findContactByEmail($email)) {
            if ($updateIfExists) {
                return $this->updateContact($acContact->id, $email, $firstName, $lastName, $phone);
            }
            else {
                return $acContact;
            }
        }

        $data['contact'] = compact('email', 'firstName', 'lastName', 'phone');
        return $this->curl('contacts', 'POST', $data);
    }

    public function updateContact($acId, $email = null, $firstName = null, $lastName = null, $phone = null)
    {
        $data['contact'] = compact('email', 'firstName', 'lastName', 'phone');
        return $this->curl('contacts/'.$acId, 'PUT', $data);
    }

    public function listContacts($params = [])
    {
        $result = $this->curl('contacts', 'GET', $params);

        if (empty($result)) return [];

        return $result->contacts;
    }

    public function findContactByEmail($email)
    {
        $contacts = $this->listContacts(['email' => $email]);

        if (empty($contacts)) return null;

        return $contacts[0];
    }

    public function deleteContact($id)
    {
        $result = $this->curl('contacts/' .  $id, 'DELETE');
        return $result;
    }

    public function createOrder($order)
    {
        $result = $this->curl('ecomOrders', 'POST', $order);
        return $result;
    }

    public function updateOrder($acOrderId, $order)
    {
        $result = $this->curl('ecomOrders/'. $acOrderId, 'PUT', $order);
        return $result;
    }

    public function listOrders($filters = [], $orderBy = [])
    {
        $params = compact('filters', 'orderBy');
        $result = $this->curl('ecomOrders', 'GET', $params);

        if (empty($result)) return [];

        return $result->ecomOrders;
    }
}
