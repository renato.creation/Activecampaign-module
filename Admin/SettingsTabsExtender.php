<?php

namespace Modules\ActiveCampaign\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;

class SettingsTabsExtender
{
    public function extend(Tabs $tabs)
    {
        $tabs->group('extras', trans('setting::settings.tabs.group.extras'))
            ->add($this->activecampaign());
    }

    private function activecampaign()
    {
        return tap(new Tab('activecampaign', trans('activecampaign::settings.tabs.activeCampaign')), function (Tab $tab) {
            $tab->weight(110);
            $tab->fields([
                'activeCampaign_enabled', 
                'activeCampaign_url',
                'activeCampaign_key',
                'activeCampaign_abandoned_cart_hours'
            ]);

            $tab->view('activecampaign::admin.settings.tabs.activecampaign');
        });
    }

}
