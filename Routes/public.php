<?php
    
Route::post('activecampaign','ActiveCampaignController@store')->name('activecampaign.index.store');

Route::get('activecampaign/index', 'ActiveCampaignController@index')->name('activecampaign.index');

Route::get('activecampaign/remove', 'ActiveCampaignController@remove')->name('activecampaign.remove');