<?php
/**
 * Created by CloudInfo.pt.
 * User: JoaoRivera
 *       joao.rivera@cloudinfo.pt
 * Date: 02/07/2020
 * Time: 19:22
 */

namespace Modules\Activecampaign\Support;


use Carbon\Carbon;
use Modules\ActiveCampaign\Entities\ActiveCampaign;
use Modules\Cart\Entities\CartStorage as CartStorageModel;
use Modules\Cart\Facades\Cart;
use Modules\User\Entities\User;

class AbandonedCart
{
    public static function sendAbandonedCarts()
    {
        if (!setting('activeCampaign_enabled')) return 0;

        $hours = setting('activeCampaign_abandoned_cart_hours',24);
        $count = 0;

        $cartSessions = CartStorageModel::where('updated_at','>',Carbon::now()->subHours($hours+24))
            ->where('updated_at','<',Carbon::now()->subHours($hours))
            ->get();

        foreach ($cartSessions as $cartSession) {
            [$userId, ,$type] = explode('_', $cartSession->id);
            if ($type === 'items' && static::createAbandonedCart($userId, $cartSession)) {
                $count++;
            }
        }
        return $count;
    }

    protected static function createAbandonedCart($userId, $cartSession)
    {
        if (!is_numeric($userId) || !($user = User::find($userId))) {
            return null;
        }

        $ac = new ActiveCampaign();
        $products = [];
        $customerId = 0;
        $connectionId = $ac->connectionId();

        Cart::session($user->id);

        $items = Cart::getContent();

        foreach ($items as $key => $item) {
            $product = $item->attributes['product'];
            $products[$key]['externalid'] = $product->id;
            $products[$key]['name'] = $item->name;
            $products[$key]['price'] = bcmul($item->price, 100);
            $products[$key]['quantity'] = $item->quantity;
        };

        $customer = $ac->findCustomerByEmail($user->email);
        if (isset($customer)) {
            $customerId = $customer->id;
        }
        else {
            $acCustomer = $ac->createCustomer([
                'id' => $user->id,
                'email' => $user->email,
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'phone' => $user->phone,
            ]);

            if (isset($acCustomer->ecomCustomer)) {
                $customerId = $acCustomer->ecomCustomer->id;
            }
        }

        $externalcheckoutid = "{$user->id}_{$cartSession->updated_at->toDateString()}";
        $abandonedCarts = $ac->listOrders([
            'connectionid' => $connectionId,
            'externalcheckoutid' => $externalcheckoutid,
        ]);

        if (empty($abandonedCarts)) {
            $data['ecomOrder'] = [
                'externalcheckoutid' => $externalcheckoutid,
                'state' => ActiveCampaign::ORDER_STATE_ABANDONED,
                'email' => $user->email,
                'orderProducts' => $products,
                'totalPrice' => bcmul(Cart::getTotal(), 100),
                'currency' => currency(),
                'connectionid' => $connectionId,
                'customerid' => $customerId,
                'abandonedDate' => $cartSession->updated_at->toDateTimeString(),
                'externalCreatedDate' => $cartSession->updated_at->toDateTimeString()
            ];

            $abandonedCart = $ac->createOrder($data);

            if (!empty($abandonedCart) && !empty($abandonedCart->ecomOrder)) {
                return $abandonedCart;
            }
        }
        return null;
    }
}