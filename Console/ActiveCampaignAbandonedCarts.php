<?php

namespace Modules\Activecampaign\Console;

use Illuminate\Console\Command;
use Modules\Activecampaign\Support\AbandonedCart;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ActiveCampaignAbandonedCarts extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'active-campaign:send-abandoned-carts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Abandoned Carts to ActiveCampaign.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = AbandonedCart::sendAbandonedCarts();

        echo "Total Abandoned Carts: $count";
    }

}
