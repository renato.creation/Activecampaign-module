<?php

return [
    'tabs' => [
        'activeCampaign' => 'ActiveCampaign'
    ],
    'form' => [
        'enable_activeCampaign' => 'Enable ActiveCampaign',
    ]
];
