<?php 

return [
    'success' => 'Thank you for subscribing to our newsletter.',
    'error' => 'Email entered is already registered.',
    'missing_email' => 'Email is missing or invalid.',
    'remove' => 'We hope you come back soon!',
];
