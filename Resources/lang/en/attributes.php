<?php

return [
    'activeCampaign_enabled' => 'Status',
    'translatable' => [
        'activeCampaign_url' => 'API Access URL',
        'activeCampaign_key' => 'API Access Key',
        'activeCampaign_abandoned_cart_hours' => 'Hours to abandon a Cart',
        'activeCampaign_header_logo' => 'Logo Image for this Connection',
    ],


];