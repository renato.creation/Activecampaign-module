<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('activeCampaign_enabled', trans('activecampaign::attributes.activeCampaign_enabled'), trans('activecampaign::settings.form.enable_activeCampaign'), $errors, $settings) }}
        {{ Form::text('activeCampaign_url', trans('activecampaign::attributes.translatable.activeCampaign_url'), $errors, $settings, ['required' => true]) }}
        {{ Form::text('activeCampaign_key', trans('activecampaign::attributes.translatable.activeCampaign_key'), $errors, $settings, ['required' => true]) }}
        {{ Form::number('activeCampaign_abandoned_cart_hours', trans('activecampaign::attributes.translatable.activeCampaign_abandoned_cart_hours'), $errors, $settings, ['required' => true]) }}

        <div class="col-md-offset-3 col-md-9">
            <div class="media-picker-divider"></div>

            @include('media::admin.image_picker.single', [
                'title' => trans('activecampaign::attributes.translatable.activeCampaign_header_logo'),
                'inputName' => 'activeCampaign_header_logo',
                'file' => \Modules\Media\Entities\File::findOrNew(setting('activeCampaign_header_logo')),
            ])
        </div>
    </div>
</div>
