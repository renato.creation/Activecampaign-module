<?php

namespace Modules\ActiveCampaign\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Page\Entities\Page;
use Modules\ActiveCampaign\Entities\ActiveCampaign;

class ActiveCampaignController extends Controller
{
    public $ac;

    public function __construct()
    {
        $this->ac = new ActiveCampaign();
    }

    public function index(Request $request){

        $my = auth()->user();
        if ($this->ac->findContactByEmail($my->email)) {
            return view('public.account.newsletter.index');
        }

        $terms_of_conditions = Page::urlForPage(setting('storefront_terms_page'));

        return view('public.account.newsletter.create', compact('my', 'terms_of_conditions'));
    }


    public function store(Request $request)
    {
        if(!$request->exists('email')) {
            return back()->withError(trans('activecampaign::messages.missing_email'));
        }

        $data['email'] = $request->get('email');
        $data['firstName'] = $request->get('name');
        $data['lastName'] = $request->get('last_name');
        $data['phone'] = $request->get('phone');

        $result = $this->ac->createCustomer($data, '1');

        if(isset($result->errors)){
            return back()->withError(trans('activecampaign::messages.error'));
        }

        return back()->withSuccess(trans('activecampaign::messages.success'));
    }


    public function remove()
    {
        $my = auth()->user();

        $customer = $this->ac->findCustomerByEmail($my->email);

        $customerId = $customer->ecomCustomers[0]->id;

        $data['ecomCustomer']['acceptsMarketing'] = '0';

        $this->ac->curl('ecomCustomers/' . $customerId , "PUT", $data);
        
        return back()->withSuccess(trans('activecampaign::messages.remove'));
            
    }

}