<?php

namespace Modules\ActiveCampaign\Providers;

use Modules\Activecampaign\Console\ActiveCampaignAbandonedCarts;
use Modules\ActiveCampaign\Listeners\CreateOrUpdateOrder;
use Modules\ActiveCampaign\Listeners\UpdateContact;
use Modules\Admin\Ui\Facades\TabManager;
use Modules\ActiveCampaign\Admin\SettingsTabsExtender;
use Illuminate\Support\ServiceProvider;
use Modules\Order\Entities\Order;
use Modules\Support\Traits\LoadsConfig;
use Modules\User\Entities\User;

class ActiveCampaignServiceProvider extends ServiceProvider
{
    use LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! config('app.installed')) {
            return;
        }

        $this->commands([
            ActiveCampaignAbandonedCarts::class,
        ]);

        User::updated(function ($user) {
            (new UpdateContact)->handle($user);
        });


        Order::updated(function ($order) {
            (new CreateOrUpdateOrder)->handle($order);
        });

        TabManager::extend('settings', SettingsTabsExtender::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
