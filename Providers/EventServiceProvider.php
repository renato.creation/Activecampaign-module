<?php

namespace Modules\ActiveCampaign\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'sentinel.registered' => [
            \Modules\ActiveCampaign\Listeners\CreateContact::class,
        ],
        \Modules\Checkout\Events\OrderPlaced::class => [
            \Modules\ActiveCampaign\Listeners\CreateOrUpdateOrder::class,
        ],
    ];
}
